# RioGrande HaveIBeenPwned API

## HaveIBeenPwned

[HaveIBeenPwned](https://haveibeenpwned.com/) is a service that checks if
accounts are part of well known breaches.

## RioGrande

RioGrande is simply the [top namespace](https://gitlab.com/ccrdude-php) 
I've picked for any PHP code I publish. It's a nerdy reference to a 
Star Trek shuttle - my top namespaces are named after spaceships.


## Abstract

Basic API v3 functionality to checked breached accounts.

[Create a new issue](/ccrdude-php/riogrande-hibp/-/issues/new)
to request new features.
