<?php

namespace RioGrande\HaveIBeenPwned\APIv3;

require_once __DIR__ . '/source/load.php';

$cfg = new Config(__DIR__ . '/config.json');
$cfg->requestRequiredCredentialsOnCommandLine();

$ba = new BreachedAccount('patrick@kolla.de', false, true);
$ba->setAPIKey($cfg->getAPIKey());
$ba->execute();
//var_dump();
//print_r($ba->getQueryResponse());
for ($i = 0; $i < $ba->getCount(); $i++) {
    $o = $ba->getData($i);
    $s = ($ba->includesPasswords($i) ? 'w/ passwords' : 'wo/ passwords');
    echo sprintf("%-20s  %10s  %10d  %s", $o->Name, $o->BreachDate, intval($o->PwnCount), $s) . "\n";
}