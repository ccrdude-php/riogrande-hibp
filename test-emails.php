<?php

namespace RioGrande\HaveIBeenPwned\APIv3;

require_once __DIR__ . '/source/load.php';

$cfg = new Config(__DIR__ . '/config.json');
$cfg->requestRequiredCredentialsOnCommandLine();

$sRawEmails = file_get_contents(__DIR__ . '/testdata/email-input.lst');
$aEmails = explode("\n", $sRawEmails);

$sFilenameData = __DIR__ . '/testdata/emails.json';
if (file_exists($sFilenameData)) {
    $aEmailData = json_decode(file_get_contents($sFilenameData), true);
} else {
    $aEmailData = array();
}


$i = 0;
foreach ($aEmails as $sEmail) {
    if (!array_key_exists($sEmail, $aEmailData)) {
        $ba = new BreachedAccount($sEmail, false, true);
        $ba->setAPIKey($cfg->getAPIKey());
        var_dump($ba->execute());
        $a = $ba->getQueryResponse();

        if (is_array($a)) {
            $aUser = array(
                'breachCount' => $ba->getCount(),
                'includesPasswords' => $ba->includesPasswords()
            );
        } else {
            $aUser = array(
                'breachCount' => 0,
                'includesPasswords' => false,
            );
        }
        $aEmailData[$sEmail] = $aUser;
        sleep(15);
        echo "{$i} ... ";
        $i++;
    }
}

$iMax = count($aEmailData);
$iBreached = 0;
$iPasswords = 0;
foreach ($aEmailData as $sEmail => $aData) {
    if ($aData['breachCount'] > 0) {
        $iBreached++;
    }
    if ($aData['includesPasswords'] == true) {
        $iPasswords++;
    }
}
if ($iMax > 0) {
    echo sprintf("Breached:   %d of %d (%.1f%s)\n", $iBreached, $iMax, ($iBreached * 100 / $iMax), '%');
    echo sprintf("Passwords:  %d of %d (%.1f%s)\n", $iPasswords, $iMax, ($iPasswords * 100 / $iMax), '%');
}

file_put_contents($sFilenameData, json_encode($aEmailData, JSON_PRETTY_PRINT));
