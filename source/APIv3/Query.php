<?php

/**
 * Implements a query to REST APIs, using CURL.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage HaveIBeenPwned
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-hibp
 * @since      0.1.0
 */

namespace RioGrande\HaveIBeenPwned\APIv3;

/**
 * Implements a query to REST APIs, using CURL.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage HaveIBeenPwned
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-hibp
 * @since      0.1.0
 */
class Query
{
    protected string $FURL;
    protected bool $FDoPost = false;
    protected array $FHeaders = [];
    protected string|array $FPostData = [];
    protected string $FCURLError = '';
    protected array $FDebugOutput = [];

    /**
     * Prepares the query with the URL to use.
     *
     * @param string $TheURL The URL to send the query to.
     */
    public function __construct(string $TheURL)
    {
        $this->FURL = $TheURL;
    }

    /**
     * Executes a query, returning null if it fails, or JSON data otherwises.
     *
     * @return array|null
     */
    public function execute(): ?array
    {
        $this->FDebugOutput['url'] = $this->FURL;
        $this->FDebugOutput['type'] = $this->FDoPost ? 'POST' : 'GET';
        $this->FDebugOutput['outgoing'] = array(
            'body' => $this->FPostData,
            'headers' => $this->FHeaders,
        );
        $ch = curl_init($this->FURL);
        try {
            curl_setopt($ch, CURLOPT_POST, $this->FDoPost);
            if ($this->FDoPost) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $this->FPostData);
            }
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->FHeaders);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            if ($response === false) {
                $this->FCURLError = curl_error($ch);
                $ret = null;
            }
            $responseData = json_decode($response, true);
            $ret = $responseData;
        } finally {
            curl_close($ch);
        }
        $this->FDebugOutput['incoming'] = array(
            'body' => $ret,
        );        
        return $ret;
    }

    /**
     * Returns debug information.
     *
     * @return array
     */
    public function getDebugOutput(): array
    {
        return $this->FDebugOutput;
    }

    /**
     * Returns a possible error text.
     *
     * @return string
     */
    public function getError(): string
    {
        return $this->FCURLError;
    }

    /**
     * Returns the URL associated with the query.
     *
     * @return string
     */
    public function getURL(): string
    {
        return $this->FURL;
    }

    /**
     * Sets headers to be used by the query.
     *
     * @param array $AHeaders A list of header entries of form "A: B".
     *
     * @return Query An instance of this to chain commands.
     */
    public function setHeaders(array $AHeaders): Query
    {
        $this->FHeaders = $AHeaders;
        return $this;
    }

    /**
     * Sets the data sent with a POST query. Changes query type to POST as well.
     *
     * @param string|array $APostData The parameters passed by POST.
     *
     * @return Query An instance of this to chain commands.
     */
    public function setPostData(string|array $APostData): Query
    {
        $this->FPostData = $APostData;
        $this->FDoPost = true;
        return $this;
    }
}
