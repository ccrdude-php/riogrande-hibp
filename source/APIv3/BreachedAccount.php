<?php

/**
 * Retrieves comments about a domain.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage HaveIBeenPwned
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-hibp
 * @since      0.1.0
 */

namespace RioGrande\HaveIBeenPwned\APIv3;

use RioGrande\HaveIBeenPwned\APIv3\Request;

/**
 * Retrieves comments about a domain.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage HaveIBeenPwned
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-hibp
 * @see        https://haveibeenpwned.com/API/v3#BreachesForAccount
 * @since      0.1.0
 */
class BreachedAccount extends Request
{
    protected string $Account;
    protected bool $TruncateResponse;
    protected bool $ReturnUnverified;

    /**
     * Initializes the BreachedAccount request.
     *
     * @param string $TheAccount    The account to query for.
     *
     * @author Patrick Kolla-ten Venne
     */
    public function __construct(string $TheAccount, bool $TruncateResponse = true, bool $ReturnUnverified = true)
    {
        $this->Account = $TheAccount;
        $this->TruncateResponse = $TruncateResponse;
        $this->ReturnUnverified = $ReturnUnverified;
        $sURL = "{$this->APIBase}/breachedaccount/{$this->Account}";
        $sURL .= '?truncateResponse=' . ($this->TruncateResponse ? 'true' : 'false');
        $sURL .= '&includeUnverified=' . ($this->ReturnUnverified ? 'true' : 'false');
        $this->setURL($sURL);
    }

    public function getCount()
    {
        return count($this->getQueryResponse());
    }

    public function getData(int $TheIndex): object
    {
        return (object)($this->getQueryResponse()[$TheIndex]);
    }

    public function includesPasswords(?int $TheIndex = null): bool
    {
        if (is_null($TheIndex)) {
            $r = false;
            for ($i = 0; $i < $this->getCount(); $i++) {
                if ($this->includesPasswords($i)) {
                    $r = true;
                    break;
                }
            }
            return $r;
        } else {
            $aData = $this->getQueryResponse();
            $a = $aData[$TheIndex]['DataClasses'];
            return (in_array('Passwords', $a));
        }
    }
}
