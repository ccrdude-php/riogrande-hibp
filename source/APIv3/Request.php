<?php

/**
 * Base class for all requests to the API.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage HaveIBeenPwned
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-hibp
 * @since      0.1.0
 */

namespace RioGrande\HaveIBeenPwned\APIv3;

use RioGrande\HaveIBeenPwned\APIv3\Query;

/**
 * Base class for all requests to the API.
 *
 * @category   API
 * @package    RioGrande
 * @subpackage HaveIBeenPwned
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-hibp
 * @since      0.1.0
 */
abstract class Request
{
    protected Query $Query;
    protected string $URL = '';
    protected string $APIKey;
    protected ?array $QueryResponse = null;
    public string $APIBase = 'https://haveibeenpwned.com/api/v3';

    /**
     * Set the value of APIKey.
     *
     * @param string $APIKey API key to pass no to VirusTotal.
     *
     * @return Request Instance of self to chain commands.
     */
    public function setAPIKey(string $APIKey): Request
    {
        $this->APIKey = $APIKey;
        return $this;
    }

    /**
     * Set the value of URL.
     *
     * @param string $URL URL to contact.
     *
     * @return Request Instance of self to chain commands.
     */
    public function setURL(string $URL): Request
    {
        $this->URL = $URL;
        unset($this->Query);
        $this->Query = new Query($URL);
        return $this;
    }

    /**
     * Get the query results.
     *
     * @return ?array
     */
    public function getQueryResponse(): ?array
    {
        return $this->QueryResponse;
    }

    /**
     * Returns debug information.
     *
     * @return array
     */
    public function getDebugOutput(): array
    {
        return $this->Query->getDebugOutput();
    }

    /**
     * Executs the DomainComments request.
     *
     * @return bool
     *
     * @author Patrick Kolla-ten Venne
     */
    public function execute(): bool
    {
        $this->Query->setHeaders(["hibp-api-key: {$this->APIKey}", "User-Agent: RioGrande/HIBP 0.1.0"]);
        $this->QueryResponse = $this->Query->execute();
        if (is_null($this->QueryResponse)) {
            return false;
        }
        return true;
    }
}
