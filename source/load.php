<?php

/**
 * Loads all required files.
 * php version 8.0
 *
 * @category   API
 * @package    RioGrande
 * @subpackage HaveIBeenPwned
 * @author     Patrick Kolla-ten Venne <patrick.kolla@safer-networking.org>
 * @license    https://en.wikipedia.org/wiki/MIT_License MIT
 * @link       https://gitlab.com/ccrdude-php/riogrande-hibp
 * @since      0.1.0
 */

namespace RioGrande\HaveIBeenPwned;

require_once __DIR__ . '/APIv3/Config.php';
require_once __DIR__ . '/APIv3/Query.php';
require_once __DIR__ . '/APIv3/Request.php';
require_once __DIR__ . '/APIv3/BreachedAccount.php';
